import { Injectable } from '@angular/core';
import { User } from '../User/user';  // Student data type interface class
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';  // Firebase modules for Database, Data list and Single object
@Injectable({
providedIn: 'root'
})
export class CrudService {
usersRef: AngularFireList<any>;    
userRef: AngularFireObject<any>;   

constructor(private db: AngularFireDatabase) { }

AddUser(user: User) {
  this.usersRef.push({
  firstName: user.firstName,
  lastName: user.lastName,
  })
  }
  // Fetch Single Student Object
  GetUser(id: string) {
  this.userRef = this.db.object('users-list/' + id);
  return this.userRef;
  }
  // Fetch Students List
  GetUsersList() {
  this.usersRef = this.db.list('users-list');
  return this.usersRef;
  }  
  // Update Student Object
  UpdateUser(user: User) {
  this.userRef.update({
  firstName: user.firstName,
  lastName: user.lastName,
  })
  }  
  // Delete Student Object
  DeleteUser(id: string) { 
  this.userRef = this.db.object('users-list/'+id);
  this.userRef.remove();
  }
  }