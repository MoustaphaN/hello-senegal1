import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './composants/login/login.component';
import { AppComponent} from './app.component';
import { AuthComponent } from './auth/auth.component';

import { UserListComponent } from './user-list/user-list.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ConnxionComponent } from './connxion/connxion.component';


const routes: Routes = [
  {path: '', redirectTo: 'connxion', pathMatch: 'full'},
  {path: 'connxion', component: ConnxionComponent},
  {path: 'auth', component:  AuthComponent},
  { path: 'login', component: LoginComponent },

  { path: 'view-users', component: UserListComponent },
  { path: 'edit-user/:id', component: EditUserComponent },
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
