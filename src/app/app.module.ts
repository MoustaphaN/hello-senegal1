import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HistoireComponent } from './composants/histoire/histoire.component';
import { HeaderComponent } from './composants/header/header.component';
import { GeographieComponent } from './composants/geographie/geographie.component';
import { PolitiqueComponent } from './composants/politique/politique.component';
import { EconomieComponent } from './composants/economie/economie.component';
import { CultureComponent } from './composants/culture/culture.component';
import { CodePipe } from './code.pipe';
import { HighlightDirective } from './highlight.directive';
import { FormsModule } from '@angular/forms';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './composants/login/login.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from 'angularfire2/auth'; 
import { environment } from '../environments/environment';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UserListComponent } from './user-list/user-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxPaginationModule } from 'ngx-pagination';
import { ConnxionComponent } from './connxion/connxion.component';
import { AuthService } from './providers/auth.service';



@NgModule({
  declarations: [
    AppComponent,
    HistoireComponent,
    HeaderComponent,
    GeographieComponent,
    PolitiqueComponent,
    EconomieComponent,
    CultureComponent,
    CodePipe,
    HighlightDirective,
    AuthComponent,
    LoginComponent,
    EditUserComponent,
    UserListComponent,
    ConnxionComponent,
    
   
    
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxPaginationModule,
    ReactiveFormsModule,

  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }

