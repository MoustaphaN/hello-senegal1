import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../login.service';


@Component({
  selector: 'app-geographie',
  templateUrl: './geographie.component.html',
  styleUrls: ['./geographie.component.css']
})
export class GeographieComponent implements OnInit {
  afficher:boolean = false;
  constructor(private loginService: LoginService) { }
  ngOnInit(){
    this.Regions=this.loginService.getvillelist();
  }
  Regions=[];

  AfficheRegion()
{ 
  if(this.afficher==false){
    return this.afficher = true;
  }else
  {
    return this.afficher = false;
  }

}
  
}
