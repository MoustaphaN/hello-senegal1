import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { CrudService } from '../../User/crud.service';    
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; 
import { ToastrService } from 'ngx-toastr';
 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public userForm: FormGroup;  
constructor(
public crudApi: CrudService, 
public fb: FormBuilder,       
public toastr: ToastrService

) { }
ngOnInit() {
this.crudApi.GetUsersList();  
this.usersForm();             
}
// Reactive student form
usersForm() {
this.userForm = this.fb.group({
  firstName: ['', [Validators.required, Validators.minLength(2)]],
  lastName: [''],
})  
}
// Accessing form control using getters
get firstName() {
return this.userForm.get('firstName');
}
get lastName() {
return this.userForm.get('lastName');
}  
ResetForm() {
this.userForm.reset();
}  
submitUserData() {
this.crudApi.AddUser(this.userForm.value);
this.toastr.success(this.userForm.controls['firstName'].value + ' successfully added!'); 
this.ResetForm();  
};
}