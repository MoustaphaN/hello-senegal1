import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnxionComponent } from './connxion.component';

describe('ConnxionComponent', () => {
  let component: ConnxionComponent;
  let fixture: ComponentFixture<ConnxionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnxionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnxionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
