import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../providers/auth.service'; 

@Component({
  selector: 'app-connxion',
  templateUrl: './connxion.component.html',
  styleUrls: ['./connxion.component.css']
})
export class ConnxionComponent implements OnInit {

model: any = {};
  constructor(public authService: AuthService, private router:Router) { }

      ngOnInit() {
      }
      login() {
        console.log('Tentative de connexion');

        // Vérifier que login/mdp sont correctes, par exemple par une requête à un service REST
        localStorage.setItem('user', JSON.stringify({login : this.model.username}));
        this.router.navigate(['/auth']);
      }
       
      logingoogle() {

        this.authService.loginWithGoogle().then((data) => {
        
        this.router.navigate(['/auth']);
        
        });
        
        }
      

}