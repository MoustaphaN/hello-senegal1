import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  private user: Observable<firebase.User>;
constructor(private _firebaseAuth: AngularFireAuth , private router: Router) { 
      this.user = _firebaseAuth.authState;
  }

  ngOnInit() {
  }

  

  

}
