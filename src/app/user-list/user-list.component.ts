import { Component, OnInit } from '@angular/core';
import { CrudService } from '../User/crud.service'; 
import { User } from './../User/user';   
import { ToastrService } from 'ngx-toastr';   

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  p: number = 1;                      
User: User[];                
hideWhenNoUser: boolean = false; 
noData: boolean = false;           
preLoader: boolean = true;          
constructor(
public crudApi: CrudService, 
public toastr: ToastrService 
){ }
ngOnInit() {
this.dataState(); 
let s = this.crudApi.GetUsersList(); 
s.snapshotChanges().subscribe(data => {
this.User = [];
data.forEach(item => {
let a = item.payload.toJSON(); 
a['$key'] = item.key;
this.User.push(a as User);
})
})
}
dataState() {     
this.crudApi.GetUsersList().valueChanges().subscribe(data => {
this.preLoader = false;
if(data.length <= 0){
this.hideWhenNoUser = false;
this.noData = true;
} else {
this.hideWhenNoUser = true;
this.noData = false;
}
})
}

deleteUser(user) {
if (window.confirm('Are sure you want to delete this user ?')) { 
this.crudApi.DeleteUser(user.$key) 
this.toastr.success(user.fi + ' successfully deleted!'); 
}
}
}
