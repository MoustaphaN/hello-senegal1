// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebase : {
    apiKey: "AIzaSyBqt8er9ip9sJpEwJ-MF5BUxvnRu5Usqjk",
    authDomain: "hello-senegal2.firebaseapp.com",
    databaseURL: "https://hello-senegal2.firebaseio.com",
    projectId: "hello-senegal2",
    storageBucket: "hello-senegal2.appspot.com",
    messagingSenderId: "1063075282756",
    appId: "1:1063075282756:web:7f090e26234bdc89"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
